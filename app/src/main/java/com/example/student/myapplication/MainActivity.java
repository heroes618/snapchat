    package com.example.student.myapplication;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import com.backendless.Backendless;

    public class MainActivity extends AppCompatActivity {

    public static final String APP_ID ="11D02515-681B-5D85-FF16-A1BE46346D00";
    public  static final String SECRET_KEY = "87FA1668-498B-5028-FF6E-1EFE6DBED500";
    public static final String VERSION = "v1";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        Backendless.initApp(this, APP_ID, SECRET_KEY, VERSION);
        if (Backendless.UserService.loggedInUser() == ""){
            MainMenuFragment mainMenu = new MainMenuFragment();
            getSupportFragmentManager().beginTransaction().add(R.id.container,mainMenu).commit();

        }else{
            HomeFragment home = new HomeFragment();
            getSupportFragmentManager().beginTransaction().add(R.id.container, home).commit();

        }

    }
}
